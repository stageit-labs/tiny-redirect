.PHONY: build upload

build:
	docker build --squash -t registry.gitlab.com/stageit-labs/tiny-redirect .
upload:
	docker push registry.gitlab.com/stageit-labs/tiny-redirect