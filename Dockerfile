############################
# STEP 1 build executable binary
############################
# golang alpine 1.12
# FROM golang@sha256:8cc1c0f534c0fef088f8fe09edc404f6ff4f729745b85deae5510bfd4c157fb2 as builder
# golang 1.12.5-alpine3.9
# FROM golang@sha256:c7330979841b518aea1cda2d02466c978b40431bbff3c60f9f0cebb18f8624d7 as builder

FROM golang:1.15.0-alpine3.12 as builder

RUN apk add upx --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community

ENV CURL_VERSION 7.72.0
RUN apk add --update --no-cache g++ perl gcc make musl-dev openssl-dev openssl-libs-static file
RUN wget https://curl.haxx.se/tiny/tiny-curl-$CURL_VERSION.tar.bz2 && \
    tar xjvf tiny-curl-$CURL_VERSION.tar.bz2 && \
    rm tiny-curl-$CURL_VERSION.tar.bz2 && \
    cd tiny-curl-$CURL_VERSION && \
    # ./configure --disable-shared --enable-static --with-ca-fallback --disable-threaded-resolver CFLAGS='-static -static-libgcc -Wl,-static -lc' && \
    ./configure --disable-shared --without-zlib --without-ssl --without-librtmp --enable-hidden-symbols --disable-unix-sockets --disable-proxy --disable-proxy --disable-manual --disable-ipv6 --disable-crypto-auth --disable-cookies --disable-ares \
        CFLAGS="-Os -ffunction-sections -fdata-sections -fno-unwind-tables -fno-asynchronous-unwind-tables -flto" \
        LDFLAGS="-Wl,-s -Wl,-Bsymbolic -Wl,--gc-sections" && \
    make && \
    make install
RUN /usr/local/bin/curl -V
RUN ldd /usr/local/bin/curl
RUN ls /usr/local/bin | grep curl
RUN upx /usr/local/bin/curl

# Install git + SSL ca certificates.
# Git is required for fetching the dependencies.
# Ca-certificates is required to call HTTPS endpoints.
RUN apk update && apk add --no-cache git ca-certificates && update-ca-certificates

# Create appuser
RUN adduser -D -g '' appuser
ADD . /app
WORKDIR /app

# Fetch dependencies.
RUN go get -d -v

# Build the binary
# RUN CGO_ENABLED=0 GOOS=linux go build -ldflags="-w -s -X main.version=2.0.3 -X main.build=`git rev-list master --first-parent --count` -X main.date=`date +%FT%T%z`" -a -installsuffix cgo -o /go/bin/astray .
RUN go build -ldflags="-X main.version=1.0.0 -X main.build=`git rev-list master --first-parent --count` -X main.date=`date +%FT%T%z`" -o /go/bin/tiny-redirect .

RUN ls /go/bin
RUN ldd /go/bin/tiny-redirect
RUN apk add upx --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community
RUN upx /go/bin/tiny-redirect

############################
# STEP 2 build a small image
############################
FROM alpine:3.12 as release
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /usr/local/bin/curl /usr/local/bin/curl
# Copy our static executable.
COPY --from=builder /go/bin/tiny-redirect /app/tiny-redirect
WORKDIR /app
EXPOSE 3000
# Run the hello binary.
ENTRYPOINT ["/app/tiny-redirect"]
