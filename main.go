package main

import (
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	logger "github.com/sirupsen/logrus"
	"gitlab.com/stageit-labs/tiny-redirect/libs"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	version string
	build   string
	date    string
)

var redirectHTTP bool
var hostCheckEnabled bool
var redirectCode int
var redirectURL string
var port string

func init() {
	if version == "" {
		version = "1.0.0"
	}
	if build == "" {
		build = "debug"
	}
	if date == "" {
		date = time.Now().UTC().String()
	}
	libs.InitLogger()
	libs.InitMetrics()
}

func main() {
	logger.Info("---------------------------------------------------")
	logger.Info("Tiny Redirect")
	logger.Infof("Version: %s", version)
	logger.Infof("Build: %s", build)
	logger.Infof("Date: %s", date)
	logger.Info("---------------------------------------------------")

	redirectURL = os.Getenv("REDIRECT_URL")
	port = os.Getenv("PORT")
	if port == "" {
		port = "3000"
	}
	var err error
	if os.Getenv("REDIRECT_CODE") != "" {
		redirectCode, err = strconv.Atoi(os.Getenv("REDIRECT_CODE"))
	}
	if err != nil || redirectCode == 0 {
		redirectCode = 302
	}
	if redirectURL == "" {
		logger.Fatal("You must specify REDIRECT_URL!!!")
	}
	//Clean up the redirect URL provided since we don't want the protocol
	if strings.HasPrefix(redirectURL, "https://") {
		redirectURL = strings.Replace(redirectURL, "https://", "", -1)
	} else if strings.HasPrefix(redirectURL, "http://") {
		redirectURL = strings.Replace(redirectURL, "http://", "", -1)
	}

	//Handle all requests
	http.HandleFunc("/", redirect)
	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	logger.Info("HTTP server started on port :" + port)
	http.ListenAndServe(":"+port, nil)
}

func redirect(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	logger.Info("Processing request from " + r.Host)
	path := r.RequestURI
	//If some passes http as a path then slap them on the hand with a bad request.
	if strings.HasPrefix(path, "/http:") || strings.HasPrefix(path, "/HTTP:") || strings.Contains(path, "comhttp") {
		logger.Info("Someone is trying to do something nasty. Returning 400.")
		http.Error(w, "Bad Request", http.StatusBadRequest)
	} else {
		//If set to use http do that.
		if !redirectHTTP {
			logger.Info("Redirecting to https://" + redirectURL)
			http.Redirect(w, r, "https://"+redirectURL, redirectCode)
		} else {
			logger.Info("Redirecting to http://" + redirectURL)
			http.Redirect(w, r, "http://"+redirectURL, redirectCode)
		}
	}
	stop := time.Now()

	entry := logger.WithFields(logrus.Fields{
		"remote_ip":     r.RemoteAddr,
		"protocol":      r.Proto,
		"host":          r.Host,
		"uri":           r.RequestURI,
		"method":        r.Method,
		"code":          strconv.Itoa(redirectCode),
		"referer":       r.Referer(),
		"user_agent":    r.UserAgent(),
		"latency":       strconv.FormatInt(stop.Sub(start).Nanoseconds(), 10),
		"latency_human": stop.Sub(start).String(),
	})
	entry.Info(r.Method + " " + path)

	proto := strconv.Itoa(r.ProtoMajor)
	proto = fmt.Sprintf("%s.%s", proto, strconv.Itoa(r.ProtoMinor))
	libs.RequestCount.WithLabelValues(proto).Inc()
	libs.RequestDuration.WithLabelValues(proto).Observe(stop.Sub(start).Seconds())
}
