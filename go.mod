module gitlab.com/stageit-labs/tiny-redirect

go 1.15

require (
	github.com/jonathan-robertson/logentrus v2.0.2+incompatible
	github.com/prometheus/client_golang v1.9.0
	github.com/sirupsen/logrus v1.7.0
)
